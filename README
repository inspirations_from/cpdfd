Code for "Prot's Dots For Debian" (PDFD)
========================================

"Prot's Dots For Debian" (PDFD) is the free/libre book that guides users
through the process of reproducing my custom BSPWM session on Debian 10
'buster' (current Debian stable).

This repo contains a fork of my dotfiles, taken from commit 90b190a4.[0]
It includes all configurations that pertain to the _previous_ iteration
of my custom desktop session, focused on BSPWM, Tmux, and Vim, as last
demonstrated in a 1.5 hour-long video published on 2019-07-02.[1]

The purpose of this repo is to provide the stable code base that is used
in PDFD's tutorials.  That free/libre book is available on my
website.[2]

State of my custom desktop session
----------------------------------

As of 2019-10-22, I am updating my dotfiles.[3] Lots of breaking changes
have already been introduced, with more to follow.  The plan is to
complete the transition from Tmux+Vim to Emacs,[4] as I have a
comprehensive setup for the latter.[5]

Users have always been warned not to track my dotfiles directly.  The
present repo, Code for PDFD, will be maintained as a separate project,
at least until the release of Debian 11 'bullseye'.

[0]: https://gitlab.com/protesilaos/dotfiles/commit/90b190a458196876ee0c20211f7ea7375bf3c0ec
[1]: https://protesilaos.com/codelog/2019-07-02-full-demo-bspwm-debian-buster/
[2]: https://protesilaos.com/pdfd
[3]: https://gitlab.com/protesilaos/dotfiles
[4]: https://protesilaos.com/codelog/2019-10-22-status-update-bspwm-emacs/
[5]: https://protesilaos.com/dotemacs
